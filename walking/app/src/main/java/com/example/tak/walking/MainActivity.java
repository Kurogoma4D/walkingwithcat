package com.example.tak.walking;

import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity implements SensorEventListener{

    private SensorManager manager;
    private Sensor delectorSensor;
    private Sensor stepCntSensor;
    private SoundPool soundPool;
    private int catSound, dogSound, tap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //センサーマネージャを取得
        manager = (SensorManager) getSystemService(SENSOR_SERVICE);

        //センサマネージャから TYPE_STEP_DETECTOR についての情報を取得する
        delectorSensor = manager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        //センサマネージャから TYPE_STEP_COUNTER についての情報を取得する
        stepCntSensor = manager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                // USAGE_MEDIA
                // USAGE_GAME
                .setUsage(AudioAttributes.USAGE_GAME)
                // CONTENT_TYPE_MUSIC
                // CONTENT_TYPE_SPEECH, etc.
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();

        soundPool = new SoundPool.Builder()
                .setAudioAttributes(audioAttributes)
                // ストリーム数に応じて
                .setMaxStreams(2)
                .build();

        // 音をロードしておく
        loadSounds();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // accuracy に変更があった時の処理
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor sensor = event.sensor;
        float[] values = event.values;
        RadioGroup radioGroup = (RadioGroup)findViewById(R.id.RadioGroup);
        int checkId = radioGroup.getCheckedRadioButtonId();

        if(sensor.getType() == Sensor.TYPE_STEP_DETECTOR){
            // ステップを検知した場合にアクセス
            Log.d("type_detector_counter", String.valueOf(values[0]));
            if(checkId != -1){
                RadioButton button = (RadioButton)findViewById(checkId);
                int num = button.getId();
                if(num == R.id.button1){
                    //猫のとき
                    soundPool.play(catSound, 1.0f, 1.0f, 0, 0, 1);
                }else if(num == R.id.button2){
                    //犬のとき
                    soundPool.play(dogSound, 1.0f, 1.0f, 0, 0, 1);
                }else if(num == R.id.button3){
                    //足音のとき
                    soundPool.play(tap, 1.0f, 1.0f, 0, 0, 1);
                }
            }else{
                //何も選択されていないとき
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // リスナー設定
        manager.registerListener (this,
                stepCntSensor,
                SensorManager.SENSOR_DELAY_NORMAL);

        manager.registerListener(this,
                delectorSensor,
                SensorManager.SENSOR_DELAY_NORMAL);

        // 音をロードしておく
        loadSounds();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // リスナー解除
        manager.unregisterListener(this,stepCntSensor);
        manager.unregisterListener(this,delectorSensor);
    }

    void loadSounds(){
        catSound = soundPool.load(this, R.raw.nc45144, 1);
        dogSound = soundPool.load(this, R.raw.dog1a, 1);
        tap = soundPool.load(this, R.raw.monster_footstep1, 1);
    }
}
